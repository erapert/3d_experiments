#include <iostream>
#include <stdlib.h>
#define _USE_MATH_DEFINES
#include <math.h>
using namespace std;

#include <GL/glew.h>
#include <GL/glut.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

#include "Logger.h"
#include "shader.hpp"
#include "fileContents.hpp"
#include "triangle.hpp"
#include "shaderProgram.hpp"
#include "texture.hpp"

// globals because building a real architecture is beyond the scope of these projects
ShaderProgram shaderProg;
SoilTexture tex;
int screenWidth = 800, screenHeight = 600;
GLuint vboCubeVerts, vboCubeUVs, iboCubeElements;
GLint attr_vertCoord, attr_texCoord;
GLint uni_mvp, uni_tex;

// global var, because a singleton is a global in disguise anyway!
Logger gLog = Logger (2, true);


int init () {
	// load up the vert and frag shaders into a complete program
	if (!shaderProg.load ("shaders/foo.vert", "shaders/foo.frag")) {
		gLog.log ("[r]!ERROR![/] Failed to init shader program:")->indent (1);
			ShaderProgram::logErrs (shaderProg.getHandle ());
		gLog.indent (-1);
		return 0;
	}
	shaderProg.connectVar (ShaderProgram::ShaderVarTypes::UNIFORM, uni_mvp, "mvp");
	shaderProg.connectVar (ShaderProgram::ShaderVarTypes::ATTRIB, attr_vertCoord, "vertcoord");
	shaderProg.connectVar (ShaderProgram::ShaderVarTypes::UNIFORM, uni_tex, "tex");
	shaderProg.connectVar (ShaderProgram::ShaderVarTypes::ATTRIB, attr_texCoord, "texcoord");

	auto textureFname = "textures/wood_cube.png";
	if (!tex.load (textureFname)) {
		gLog.log ("[r]!ERROR![/] Failed to load texture: \"%s\"", textureFname);
		return 0;
	}

	// cube verts
	GLfloat cubeVerts [] = {
		// front
		-1.0, -1.0,  1.0,
		1.0, -1.0,  1.0,
		1.0,  1.0,  1.0,
		-1.0,  1.0,  1.0,
		// top
		-1.0,  1.0,  1.0,
		1.0,  1.0,  1.0,
		1.0,  1.0, -1.0,
		-1.0,  1.0, -1.0,
		// back
		1.0, -1.0, -1.0,
		-1.0, -1.0, -1.0,
		-1.0,  1.0, -1.0,
		1.0,  1.0, -1.0,
		// bottom
		-1.0, -1.0, -1.0,
		1.0, -1.0, -1.0,
		1.0, -1.0,  1.0,
		-1.0, -1.0,  1.0,
		// left
		-1.0, -1.0, -1.0,
		-1.0, -1.0,  1.0,
		-1.0,  1.0,  1.0,
		-1.0,  1.0, -1.0,
		// right
		1.0, -1.0,  1.0,
		1.0, -1.0, -1.0,
		1.0,  1.0, -1.0,
		1.0,  1.0,  1.0
	};
	glGenBuffers (1, &vboCubeVerts);
	glBindBuffer (GL_ARRAY_BUFFER, vboCubeVerts);
	glBufferData (GL_ARRAY_BUFFER, sizeof(cubeVerts), cubeVerts, GL_STATIC_DRAW);

	// 2 floats per uv, 4 verts per face, 6 faces
	GLfloat cubeUVs [2 * 4 * 6] = {
		// front
		0.0, 0.0,
		1.0, 0.0,
		1.0, 1.0,
		0.0, 1.0
	};
	for (auto i = 1; i < 6; ++i) {
		memcpy (&cubeUVs[i * 4 * 2], &cubeUVs[0], (2 * 4 * sizeof (GLfloat)));
	}
	glGenBuffers (1, &vboCubeUVs);
	glBindBuffer (GL_ARRAY_BUFFER, vboCubeUVs);
	glBufferData (GL_ARRAY_BUFFER, sizeof(cubeUVs), cubeUVs, GL_STATIC_DRAW);

	// indexes to the verts define what order to use the verts to draw the object
	// each triplet defines a tri
	const auto numFaces = 6;
	const auto numVerts = 3;			// verts per face
	const auto numTris = numFaces * 2;	// 2 tris per quad (face)
	const auto numElements = numVerts * numFaces;
	GLushort cubeElements [numElements];
	auto i = 0;
	auto stride = 0;
	while (stride < numElements) {
		cubeElements [stride] = cubeElements [stride + 5] = i;
		cubeElements [stride + 1] = i + 1;
		cubeElements [stride + 2] = cubeElements [stride + 3] = i + 2;
		cubeElements [stride + 4] = i + 3;

		stride += 6;
		i += 4;
	}
	glGenBuffers (1, &iboCubeElements);
	glBindBuffer (GL_ELEMENT_ARRAY_BUFFER, iboCubeElements);
	glBufferData (GL_ELEMENT_ARRAY_BUFFER, sizeof (cubeElements), cubeElements, GL_STATIC_DRAW);

	return 1;
}

void onDisplay () {
	glClearColor (0.5f, 0.5f, 0.5f, 1.0);
	glClear (GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glEnable (GL_BLEND | GL_DEPTH_TEST | GL_TEXTURE_2D);
	glBlendFunc (GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	glUseProgram (shaderProg.getHandle ());

	// verts and uv's
	glBindBuffer (GL_ARRAY_BUFFER, vboCubeVerts);
	glEnableVertexAttribArray (attr_vertCoord);
	glVertexAttribPointer (attr_vertCoord, 3, GL_FLOAT, GL_FALSE, 0, 0);

	glBindBuffer (GL_ARRAY_BUFFER, vboCubeUVs);
	glEnableVertexAttribArray (attr_texCoord);
	glVertexAttribPointer (attr_texCoord, 2, GL_FLOAT, GL_FALSE, 0, 0);

	glActiveTexture (GL_TEXTURE0);
	glBindTexture (GL_TEXTURE_2D, tex.getHandle ());
	glUniform1i (uni_tex, 0);

	// draw
	glBindBuffer (GL_ELEMENT_ARRAY_BUFFER, iboCubeElements);
	GLint elemSize; glGetBufferParameteriv (GL_ELEMENT_ARRAY_BUFFER, GL_BUFFER_SIZE, &elemSize);
	glDrawElements (GL_TRIANGLES, (elemSize / sizeof (GLushort)), GL_UNSIGNED_SHORT, 0);

	// done drawing
	glDisableVertexAttribArray (attr_vertCoord);
	glDisableVertexAttribArray (attr_texCoord);
	glBindBuffer (GL_ARRAY_BUFFER, 0);
	glutSwapBuffers ();
}

void onThink () {
	auto elapsedTime = glutGet (GLUT_ELAPSED_TIME);
	auto angle = elapsedTime / 1000.0f * 15;
	//glm::mat4
	auto rotAnim = \
		glm::rotate (glm::mat4 (1.0f), angle * 3.0f, glm::vec3 (1, 0, 0)) *
		glm::rotate (glm::mat4 (1.0f), angle * 2.0f, glm::vec3 (0, 1, 0)) *
		glm::rotate (glm::mat4 (1.0f), angle * 1.0f, glm::vec3 (0, 0, 1));
	auto model = glm::translate (glm::mat4 (1.0f), glm::vec3 (0.0f, 0.0f, -4.0f));
	auto view = glm::lookAt (glm::vec3 (0.0f, 2.0f, 0.0f), glm::vec3 (0.0f, 0.0f, -4.0f), glm::vec3 (0.0f, 1.0f, 0.0f));
	auto projection = glm::perspective (45.0f, 1.0f * screenWidth / screenHeight, 0.1f, 10.0f);
	auto mvp = projection * view * model * rotAnim;

	glUseProgram (shaderProg.getHandle ());
	glUniformMatrix4fv (uni_mvp, 1, GL_FALSE, glm::value_ptr (mvp));
	
	glutPostRedisplay ();
}

void onResize (int width, int height) {
	screenWidth = width;
	screenHeight = height;
	glViewport (0, 0, screenWidth, screenHeight);
}

void shutdown () {
	//glDeleteProgram (shaderProg.getHandle ());
	shaderProg.unload ();
	tex.unload ();
	glDeleteBuffers (1, &vboCubeVerts);
	glDeleteBuffers (1, &iboCubeElements);
}

int main (int argc, char ** argv) {
	glutInit (&argc, argv);
	glutInitDisplayMode (GLUT_RGBA | GLUT_ALPHA | GLUT_DOUBLE | GLUT_DEPTH);
	glutInitWindowSize (screenWidth, screenHeight);
	glutCreateWindow ("OGL glut Demo");

	GLenum glew_status = glewInit ();
	if (glew_status != GLEW_OK) {
		gLog.log ("[r]!ERROR![/] %s", glewGetErrorString (glew_status));
		return 1;
	}

	if (!GLEW_VERSION_2_0) {
		gLog.log ("[r]!ERROR![/] Your GPU doesn't support OpenGL 2.0");
		return 1;
	}

	if (init () == 1) {
		glutDisplayFunc (onDisplay);
		glutIdleFunc (onThink);
		glutReshapeFunc (onResize);
		glutMainLoop ();
	}

	shutdown ();

	return 0;
}
