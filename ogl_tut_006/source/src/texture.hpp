#ifndef TEXTURE_H
#define TEXTURE_H 1

#include <GL/glew.h>
#include <GL/glut.h>

/*
	wrapper around SOIL for handling textures
	SOIL library: http://www.lonesock.net/soil.html
*/

class SoilTexture {
	public:
						SoilTexture		();
						~SoilTexture	();

		bool			load			(const char * filename);
		bool			unload			();
		GLuint			getHandle		() const;
		int				width			() const;
		int				height			() const;

	private:
						SoilTexture		(const SoilTexture & other);
		SoilTexture *	copy			(const SoilTexture & other);
		SoilTexture *	operator =		(const SoilTexture & other);
	
	private:
						GLuint			texId;	// handle to the texture on the GPU
						int				w;		// width
						int				h;		// height
						unsigned char *	img;	// holds the actual data of the image
};

inline GLuint SoilTexture::getHandle () const {
	return texId;
}

inline int SoilTexture::width () const {
	return w;
}

inline int SoilTexture::height () const {
	return h;
}

#endif
