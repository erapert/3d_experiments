ogl_tut_006
============

A very simple implementation following the tutorial [here](http://en.wikibooks.org/wiki/OpenGL_Programming/Modern_OpenGL_Tutorial_06).

###Features
* abstraction of shader program
    + allows binding C++ vars to GLSL vars
    + handles shaders (vert and frag)
* file loader for loading shaders written in separate files
* trivial abstraction of a colored vertex (used for building a triangle)
* texture!