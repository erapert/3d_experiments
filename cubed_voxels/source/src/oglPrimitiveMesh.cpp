#include "oglPrimitiveMesh.hpp"

OGLPrimitiveMesh::OGLPrimitiveMesh () : numElements(0) {
	for (int i = 0; i < MAX_HANDLES; ++i) {
		handles[i] = 0;
	}
}

OGLPrimitiveMesh::~OGLPrimitiveMesh () {
	unload ();
}

void OGLPrimitiveMesh::draw () {
}

bool OGLPrimitiveMesh::unload () {
	glDeleteBuffers (2, handles);
	return true;
}
