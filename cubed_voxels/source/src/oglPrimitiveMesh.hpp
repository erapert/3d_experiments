#ifndef PRIMITIVE_MESH_H
#define PRIMITIVE_MESH_H 1

#include "shaderProgram.hpp"
#include "oglFundamentals.hpp"

class OGLPrimitiveMesh {
	public:
		enum HandleInds { HVERTS, HELEMS, MAX_HANDLES };

	public:
								OGLPrimitiveMesh				();
								~OGLPrimitiveMesh				();

		virtual bool			load							() = 0;
		virtual void			draw							();
		bool					unload							();

		GLuint *				getHandles						();
		GLuint					getNumElements					();

	protected:
		GLuint					handles [MAX_HANDLES]; // handles to the vert and element buffers on the GPU
		GLuint					numElements;
};

inline GLuint * OGLPrimitiveMesh::getHandles () {
	return handles;
}

inline GLuint OGLPrimitiveMesh::getNumElements () {
	return numElements;
}

#endif
