#include "oglPrimitiveCube.hpp"

PrimitiveCube::PrimitiveCube (const float & sideLength) : OGLPrimitiveMesh(), sideLen(sideLength){
}

PrimitiveCube::~PrimitiveCube () {
}

bool PrimitiveCube::load () {
	glGenBuffers (2, handles);

	Vertex verts [8] = {
		// front
		{-sideLen, -sideLen,  sideLen},
		{sideLen, -sideLen,  sideLen},
		{sideLen,  sideLen,  sideLen},
		{-sideLen,  sideLen,  sideLen},
		// back
		{-sideLen, -sideLen, -sideLen},
		{sideLen, -sideLen, -sideLen},
		{sideLen,  sideLen, -sideLen},
		{-sideLen,  sideLen, -sideLen}
	};
	glBindBuffer (GL_ARRAY_BUFFER, handles[HVERTS]);
	glBufferData (GL_ARRAY_BUFFER, sizeof(verts), verts, GL_STATIC_DRAW);
	glBindBuffer (GL_ARRAY_BUFFER, 0);

	GLushort elements [] = {
		// front
		0, 1, 2, 2, 3, 0,
		// top
		3, 2, 6, 6, 7, 3,
		// back
		7, 6, 5, 5, 4, 7,
		// bottom
		4, 5, 1, 1, 0, 4,
		// left
		4, 0, 3, 3, 7, 4,
		// right
		1, 5, 6, 6, 2, 1
	};
	glBindBuffer (GL_ELEMENT_ARRAY_BUFFER, handles[HELEMS]);
	glBufferData (GL_ELEMENT_ARRAY_BUFFER, sizeof(elements), elements, GL_STATIC_DRAW);
	glBindBuffer (GL_ELEMENT_ARRAY_BUFFER, 0);

	numElements = sizeof (elements) / sizeof (GLushort);
	return true;
}
