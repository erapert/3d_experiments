#include <iostream>
#include <stdlib.h>
#define _USE_MATH_DEFINES
#include <math.h>
using namespace std;

#include <GL/glew.h>
#include <GL/glut.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

#include "Logger.h"
#include "shader.hpp"
#include "fileContents.hpp"
#include "oglFundamentals.hpp"
#include "shaderProgram.hpp"
#include "texture.hpp"
#include "oglPrimitiveCube.hpp"

// globals because building a real architecture is beyond the scope of these projects
int screenWidth = 800, screenHeight = 600;
GLuint vboCubeVerts, vboCubeUVs, iboCubeElements;
GLint attr_vertCoord, attr_texCoord;
GLint uni_mvp, uni_tex;

ShaderProgram shaderProg;
SoilTexture tex;
auto numPrims = 1;
OGLPrimitiveMesh ** prims; // we'll just loop through 'em and have them draw themselves

// global var, because a singleton is a global in disguise anyway!
Logger gLog = Logger (2, true);


int init () {
	// load up the vert and frag shaders into a complete program
	if (!shaderProg.load ("shaders/foo.vert", "shaders/foo.frag")) {
		gLog.log ("[r]!ERROR![/] Failed to init shader program:")->indent (1);
			ShaderProgram::logErrs (shaderProg.getHandle ());
		gLog.indent (-1);
		return 0;
	}
	shaderProg.connectVar (ShaderProgram::ShaderVarTypes::UNIFORM, uni_mvp, "mvp");
	shaderProg.connectVar (ShaderProgram::ShaderVarTypes::ATTRIB, attr_vertCoord, "vertcoord");
	shaderProg.connectVar (ShaderProgram::ShaderVarTypes::UNIFORM, uni_tex, "tex");
	shaderProg.connectVar (ShaderProgram::ShaderVarTypes::ATTRIB, attr_texCoord, "texcoord");

	auto textureFname = "textures/wood_cube.png";
	if (!tex.load (textureFname)) {
		gLog.log ("[r]!ERROR![/] Failed to load texture: \"%s\"", textureFname);
		return 0;
	}

	prims = new OGLPrimitiveMesh * [numPrims];
	for (auto i = 0; i < numPrims; ++i) {
		prims [i] = new PrimitiveCube;
		prims [i]->load ();
	}

	return 1;
}

void onDisplay () {
	glClearColor (0.5f, 0.5f, 0.5f, 1.0);
	glClear (GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glEnable (GL_BLEND | GL_DEPTH_TEST | GL_TEXTURE_2D);
	glBlendFunc (GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	glUseProgram (shaderProg.getHandle ());
	for (auto i = 0; i < numPrims; ++i) {
		auto handles = prims[i]->getHandles();
		glBindBuffer (GL_ARRAY_BUFFER, handles[OGLPrimitiveMesh::HandleInds::HVERTS]);
		glEnableVertexAttribArray (attr_vertCoord);
		glVertexAttribPointer (attr_vertCoord, 3, GL_FLOAT, GL_FALSE, 0, 0);

		glBindBuffer (GL_ELEMENT_ARRAY_BUFFER, handles[OGLPrimitiveMesh::HandleInds::HELEMS]);
		glDrawElements (GL_TRIANGLES, prims[i]->getNumElements(), GL_UNSIGNED_SHORT, 0);
	}
	glBindBuffer (GL_ARRAY_BUFFER, 0);
	glutSwapBuffers ();
}

void onThink () {
	auto elapsedTime = glutGet (GLUT_ELAPSED_TIME);
	auto angle = elapsedTime / 1000.0f * 15;
	//glm::mat4
	auto rotAnim = \
		glm::rotate (glm::mat4 (1.0f), angle * 3.0f, glm::vec3 (1, 0, 0)) *
		glm::rotate (glm::mat4 (1.0f), angle * 2.0f, glm::vec3 (0, 1, 0)) *
		glm::rotate (glm::mat4 (1.0f), angle * 1.0f, glm::vec3 (0, 0, 1));
	auto model = glm::translate (glm::mat4 (1.0f), glm::vec3 (0.0f, 0.0f, -4.0f));
	auto view = glm::lookAt (glm::vec3 (0.0f, 2.0f, 0.0f), glm::vec3 (0.0f, 0.0f, -4.0f), glm::vec3 (0.0f, 1.0f, 0.0f));
	auto projection = glm::perspective (45.0f, 1.0f * screenWidth / screenHeight, 0.1f, 10.0f);
	auto mvp = projection * view * model * rotAnim;

	glUseProgram (shaderProg.getHandle ());
	glUniformMatrix4fv (uni_mvp, 1, GL_FALSE, glm::value_ptr (mvp));
	
	glutPostRedisplay ();
}

void onResize (int width, int height) {
	screenWidth = width;
	screenHeight = height;
	glViewport (0, 0, screenWidth, screenHeight);
}

void shutdown () {
	//glDeleteProgram (shaderProg.getHandle ());
	shaderProg.unload ();
	tex.unload ();

	for (auto i = 0; i < numPrims; ++i) {
		delete prims [i];
		prims [i] = nullptr;
	}
	delete [] prims;
}

int main (int argc, char ** argv) {
	glutInit (&argc, argv);
	glutInitDisplayMode (GLUT_RGBA | GLUT_ALPHA | GLUT_DOUBLE | GLUT_DEPTH);
	glutInitWindowSize (screenWidth, screenHeight);
	glutCreateWindow ("OGL glut Demo");

	GLenum glew_status = glewInit ();
	if (glew_status != GLEW_OK) {
		gLog.log ("[r]!ERROR![/] %s", glewGetErrorString (glew_status));
		return 1;
	}

	if (!GLEW_VERSION_2_0) {
		gLog.log ("[r]!ERROR![/] Your GPU doesn't support OpenGL 2.0");
		return 1;
	}

	if (init () == 1) {
		glutDisplayFunc (onDisplay);
		glutIdleFunc (onThink);
		glutReshapeFunc (onResize);
		glutMainLoop ();
	}

	shutdown ();

	return 0;
}
