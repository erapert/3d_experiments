#ifndef PRIMITIVE_CUBE_H
#define PRIMITIVE_CUBE_H 1

#include <GL/glew.h>
#include <GL/glut.h>

#include "oglFundamentals.hpp"
#include "oglPrimitiveMesh.hpp"

class PrimitiveCube : public OGLPrimitiveMesh {
	public:
						PrimitiveCube		(const float & sideLength = 0.5f);
						~PrimitiveCube		();

		virtual bool	load				();

	private:
		float			sideLen;
};

#endif
