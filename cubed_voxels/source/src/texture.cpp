#include <SOIL.h>
#include "texture.hpp"

SoilTexture::SoilTexture () : w(0), h(0), img(nullptr) {
}

SoilTexture::~SoilTexture () {
	unload ();
}

bool SoilTexture::load (const char * filename) {
	unload ();
	// load to main RAM
	img = SOIL_load_image (filename, &w, &h, nullptr, 0);
	if (!img) { return false; }
	
	// send to GPU
	glGenTextures (1, &texId);
	glBindTexture (GL_TEXTURE_2D, texId);
	glTexImage2D (GL_TEXTURE_2D, 0, GL_RGB, w, h, 0, GL_RGB, GL_UNSIGNED_BYTE, img);
	glTexParameteri (GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri (GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameterf (GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameterf (GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

	// free from main RAM
	SOIL_free_image_data (img);
	img = nullptr;
	return true;
}

bool SoilTexture::unload () {
	// free from GPU
	glDeleteTextures (1, &texId);
	return true;
}
