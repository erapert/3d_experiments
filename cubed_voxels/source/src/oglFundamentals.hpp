#ifndef FUNDAMENTALS_H
#define FUNDAMENTALS_H 1

#include <GL/glew.h>
#include <GL/glut.h>

/*
	for now this is just a bunch of utility stuff
*/

// a real mesh wouldn't be storing color per-vertex
// but for now it's convenient
struct ColorVertex {
	GLfloat coords [3];
	GLfloat colors [3];
	// Remember, static members do not affect memory layout! So these members
	// are not included in the stride size.
	static const unsigned int coordStride = sizeof (GLfloat) * 6;
	static const unsigned int coordOffset = 0;
	static const unsigned int colorStride = sizeof (GLfloat) * 6;
	static const unsigned int colorOffset = sizeof (GLfloat) * 3;
};

struct TexVertex {
	GLfloat coords [3];
	GLfloat uvs [2];
	static const unsigned int coordStride = sizeof (GLfloat) * 5;
	static const unsigned int coordOffset = 0;
	static const unsigned int uvStride = sizeof (GLfloat) * 5;
	static const unsigned int uvOffset = sizeof (GLfloat) * 3;
};

struct Vertex {
	GLfloat coords [3];
	static const unsigned int coordStride = sizeof (GLfloat) * 3;
	static const unsigned int coordOffset = 0;

	void set (GLfloat x, GLfloat y, GLfloat z) {
		coords [0] = x;
		coords [1] = y;
		coords [2] = z;
	}
	GLfloat x () { return coords[0]; }
	GLfloat y () { return coords[1]; }
	GLfloat z () { return coords[2]; }
	void x (const GLfloat & v) { coords[0] = v; }
	void y (const GLfloat & v) { coords[1] = v; }
	void z (const GLfloat & v) { coords[2] = v; }
};

struct UV {
	GLfloat uvs [2];
	static const unsigned int uvStride = sizeof (GLfloat) * 2;
	static const unsigned int uvOffset = 0;
};

#endif
