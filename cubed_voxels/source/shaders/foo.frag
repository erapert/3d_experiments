#version 120

uniform sampler2D tex;
// comes in from the vert shader
varying vec2 f_texcoord;

void main () {
	gl_FragColor = texture2D (tex, f_texcoord);
}
