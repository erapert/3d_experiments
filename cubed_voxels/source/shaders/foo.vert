#version 120

uniform mat4 mvp;

attribute vec3 vertcoord;
attribute vec2 texcoord;
varying vec2 f_texcoord;

void main () {
	f_texcoord = texcoord;
	gl_Position = mvp * vec4 (vertcoord, 1.0);
}
