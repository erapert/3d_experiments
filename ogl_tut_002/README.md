ogl_tut_002
============
A very simple implementation following the [tutorial here](http://en.wikibooks.org/wiki/OpenGL_Programming/Modern_OpenGL_Tutorial_02)

###Features
* shader class for handling shaders (not entire shader programs though!)
* file loader for loading shaders written in separate files

Yippee!