#include <iostream>
#include <stdlib.h>
using namespace std;

#include <GL/glew.h>
#include <GL/glut.h>

#include "Logger.h"
#include "shader.hpp"
#include "fileContents.hpp"

GLuint shaderProgram;
GLuint vboTri;
GLint attributeCoord2d;

// global var, because a singleton is a global in disguise anyway!
Logger gLog = Logger (2, true);



int init () {
	GLint shaderInittedOK = GL_FALSE;

	string vertShadFileName ("shaders/foo.vert");
	string fragShadFileName ("shaders/foo.frag");
	FileContents fc;
	Shader foovert;
	Shader foofrag;

	if (!fc.load (vertShadFileName) || !foovert.load (fc.getContents(), GL_VERTEX_SHADER)) {
		gLog.log ("[r]!ERROR![/] Failed to load shader file '%s'", vertShadFileName);
	}
	if (!fc.load (fragShadFileName) || !foofrag.load (fc.getContents(), GL_FRAGMENT_SHADER)) {
		gLog.log ("[r]!ERROR![/] Failed to load shader file '%s'", fragShadFileName);
	}
	
	// the shader program is a complete chain of vertex to fragment shaders
	shaderProgram = glCreateProgram ();
	glAttachShader (shaderProgram, foovert.getHandle());
	glAttachShader (shaderProgram, foofrag.getHandle());
	glLinkProgram (shaderProgram);
	glGetProgramiv (shaderProgram, GL_LINK_STATUS, &shaderInittedOK);
	if (!shaderInittedOK) {
		cout << "!ERROR! Failed to link GL shader program." << endl;
		return 0;
	}

	// triangle verts
	GLfloat triVerts [] = {
		0.0f, 0.8f,
		-0.8f, -0.8f,
		0.8f, -0.8f
	};
	glGenBuffers (1, &vboTri);				// make a buffer on the GPU and hook up vboTri as the handle
	glBindBuffer (GL_ARRAY_BUFFER, vboTri); // make vboTri the active buffer
	glBufferData (GL_ARRAY_BUFFER, sizeof(triVerts), triVerts, GL_STATIC_DRAW); // send the verts over to the GPU
	glBindBuffer (GL_ARRAY_BUFFER, 0);		// unset the active buffer (i.e. in case we're uploading more verts later)

	return 1;
}

void onDisplay () {
	glClearColor (1.0, 1.0, 1.0, 1.0);
	glClear (GL_COLOR_BUFFER_BIT);

	glUseProgram (shaderProgram);
	glBindBuffer (GL_ARRAY_BUFFER, vboTri);
	glEnableVertexAttribArray (attributeCoord2d);
	glVertexAttribPointer (
		attributeCoord2d,		// the attr
		2,						// number of elements per vertex
		GL_FLOAT,				// type of each element
		GL_FALSE,				// take my values as they are
		0,						// no extra data between each vert
		0						// the buffer of verts
	);

	glDrawArrays (GL_TRIANGLES, 0, 3);
	glDisableVertexAttribArray (attributeCoord2d);
	glutSwapBuffers ();
}

void shutdown () {
	glDeleteProgram (shaderProgram);
	glDeleteBuffers (1, &vboTri);
}

int main (int argc, char ** argv) {
	glutInit (&argc, argv);
	glutInitDisplayMode (GLUT_RGBA | GLUT_DOUBLE | GLUT_DEPTH);
	glutInitWindowSize (640, 480);
	glutCreateWindow ("OGL glut Demo");

	GLenum glew_status = glewInit ();
	if (glew_status != GLEW_OK) {
		gLog.log ("[r]!ERROR![/] %s", glewGetErrorString (glew_status));
		return 1;
	}

	if (!GLEW_VERSION_2_0) {
		gLog.log ("[r]!ERROR![/] Your GPU doesn't support OpenGL 2.0");
		return 1;
	}

	if (init () == 1) {
		glutDisplayFunc (onDisplay);
		glutMainLoop ();
	}

	shutdown ();

	return 0;
}
