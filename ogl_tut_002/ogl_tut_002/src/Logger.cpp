#include <stdio.h>
#include <string.h>
#include <cstdarg>
#include "Logger.h"
using namespace std;

const Logger::ColorCode Logger::colors [Logger::numColors] = {
	{"[.]", "\033[90m", "grey"},
	{"[r]", "\033[91m", "red"},
	{"[g]", "\033[92m", "green"},
	{"[y]", "\033[93m", "yellow"},
	{"[b]", "\033[94m", "blue"},
	{"[m]", "\033[95m", "magenta"},
	{"[c]", "\033[96m", "cyan"},
	{"[w]", "\033[97m", "white"},
	{"[/]", "\033[0m", "end color"}
};

#ifdef LOGGING_ENABLED
	Logger::Logger (unsigned int indentSize, bool colorOn) {
		ind = 0;
		indSize = indentSize;
		enableColor (colorOn);
	}

	Logger::~Logger () {
	}

	Logger * Logger::log (const char * fmt, ...) {
		auto istr = getIndentStr ();
		printf (istr.c_str ());
	
		va_list args;
		va_start (args, fmt);
		if (colorEnabled) {
			string msg (fmt);
			msg = colorize (msg);
			vprintf (msg.c_str(), args);
		} else {
			vprintf (fmt, args);
		}
		va_end (args);
		#ifdef WIN32
			printf ("\n");
		#else
			printf ("%s\n", colors [numColors - 1].code);
		#endif
		return this;
	}

	Logger * Logger::log (const string & fmt, ...) {
		auto istr = getIndentStr ();
		printf (istr.c_str ());
	
		va_list args;
		va_start (args, fmt);
		if (colorEnabled) {
			string msg (colorize (fmt));
			vprintf (msg.c_str (), args);
		} else {
			vprintf (fmt.c_str (), args);
		}
		va_end (args);
		#ifdef WIN32
			printf ("\n");
		#else
			printf ("%s\n", colors [numColors - 1].code);
		#endif
		return this;
	}

	string Logger::getIndentStr () {
		string rtrn;
		for (unsigned int i = 0; i < (ind * indSize); ++i) {
			rtrn += " ";
		}
		return rtrn;
	}

	string Logger::colorize (const string & str) {
		string rtrn (str);
		for (unsigned int i = 0; i < numColors; ++i) {
			auto taglen = strlen (colors[i].tag);
			auto pos = -1;
			do {
				pos = rtrn.find (colors[i].tag);
				if (pos > -1) { rtrn.replace (pos, taglen, colors[i].code); }
			} while (pos != -1);
		}
		return rtrn;
	}

	void Logger::showColorCodes () {
	}
#endif
