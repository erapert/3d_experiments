#include <fstream>
#include "fileContents.hpp"
using namespace std;

FileContents::FileContents () {
}

FileContents::FileContents (const FileContents & other) {
	copy (other);
}

FileContents::~FileContents () {
	// contents will clean itself up
}

bool FileContents::load (const char * fname) {
	ifstream fil (fname);
	if (!fil.is_open ()) { return false; }
	fil.seekg (0, std::ios::end);
	auto fsize = fil.tellg ();
	contents.resize (fsize.operator+(1));
	fil.seekg (0);
	fil.read (&contents[0], fsize);
	fil.close ();
	return true;
}

bool FileContents::save (const char * fname) {
	ofstream fil (fname);
	if (!fil.is_open ()) { return false; }
	fil << contents.c_str ();
	fil.close ();
	return true;
}
