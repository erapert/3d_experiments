ogl_tut_005
============

__(ogl_tut_004 is combined with ogl_tut_003)__

A very simple implementation following the tutorial [here](http://en.wikibooks.org/wiki/OpenGL_Programming/Modern_OpenGL_Tutorial_05).

###Features
* shader class for handling shaders (not entire shader programs though!)
* file loader for loading shaders written in separate files
* trivial abstraction of a colored vertex (used for building a triangle)
* trivial function for binding C++ variables to handles of shader variables (uniforms and attributes)
* animation: the cube rotates in place

Yippee!