#include <iostream>
#include <stdlib.h>
#define _USE_MATH_DEFINES
#include <math.h>
using namespace std;

#include <GL/glew.h>
#include <GL/glut.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

#include "Logger.h"
#include "shader.hpp"
#include "fileContents.hpp"
#include "triangle.hpp"
#include "shaderProgram.hpp"

// globals because building a real architecture is beyond the scope of these projects
ShaderProgram shaderProg;
int screenWidth = 800, screenHeight = 600;
GLuint vboCube, iboCubeElements;;
GLint attr_coord3d, attr_vcolor, uni_mvp;

// global var, because a singleton is a global in disguise anyway!
Logger gLog = Logger (2, true);


int init () {
	GLint shaderInittedOK = GL_FALSE;

	// load up the vert and frag shaders into a complete program
	if (!shaderProg.load ("shaders/foo.vert", "shaders/foo.frag")) {
		gLog.log ("[r]!ERROR![/] Failed to init shader program.")->indent (1);
			ShaderProgram::logErrs (shaderProg.getHandle ());
		gLog.indent (-1);
		return 0;
	}
	/*
	attachVarToShader (ATTRIB, attr_coord3d, shaderProg.getHandle (), "coord3d");
	attachVarToShader (ATTRIB, attr_vcolor, shaderProg.getHandle (), "vcolor");
	attachVarToShader (UNIFORM, uni_mvp, shaderProg.getHandle (), "mvp");
	*/
	shaderProg.connectVar (ShaderProgram::ShaderVarTypes::ATTRIB, attr_coord3d, "coord3d");
	shaderProg.connectVar (ShaderProgram::ShaderVarTypes::ATTRIB, attr_vcolor, "vcolor");
	shaderProg.connectVar (ShaderProgram::ShaderVarTypes::UNIFORM, uni_mvp, "mvp");

	// cube verts
	struct ColorVertex cube [] = {
		// front
		{{-0.5f, -0.5f, 0.5f}, {1.0f, 0.0f, 0.0f}},
		{{0.5f, -0.5f, 0.5f}, {0.0f, 0.0f, 1.0f}},
		{{0.5f, 0.5f, 0.5f}, {0.0f, 1.0f, 0.0f}},
		{{-0.5f, 0.5f, 0.5f}, {0.0f, 0.0f, 0.0f}},
		// back
		{{-0.5f, -0.5f, -0.5f}, {0.0f, 0.0f, 1.0f}},
		{{0.5f, -0.5f, -0.5f}, {0.0f, 1.0f, 0.0f}},
		{{0.5f, 0.5f, -0.5f}, {1.0f, 0.0f, 0.0f}},
		{{-0.5f, 0.5f, -0.5f}, {0.0f, 0.0f, 0.0f}}
	};
	glGenBuffers (1, &vboCube);
	glBindBuffer (GL_ARRAY_BUFFER, vboCube);
	glBufferData (GL_ARRAY_BUFFER, sizeof(cube), cube, GL_STATIC_DRAW);

	// indexes to the verts define what order to use the verts to draw the object
	// each triplet defines a tri
	GLushort cubeElements [] = {
		// front
		0, 1, 2,
		2, 3, 0,
		// top
		3, 2, 6,
		6, 7, 3,
		// back
		7, 6, 5,
		5, 4, 7,
		// bottom
		4, 5, 1,
		1, 0, 4,
		// left
		4, 0, 3,
		3, 7, 4,
		// right
		1, 5, 6,
		6, 2, 1
	};
	glGenBuffers (1, &iboCubeElements);
	glBindBuffer (GL_ELEMENT_ARRAY_BUFFER, iboCubeElements);
	glBufferData (GL_ELEMENT_ARRAY_BUFFER, sizeof (cubeElements), cubeElements, GL_STATIC_DRAW);
	glBindBuffer (GL_ELEMENT_ARRAY_BUFFER, 0);

	return 1;
}

void onDisplay () {
	glClearColor (0.5f, 0.5f, 0.5f, 1.0);
	glEnable (GL_DEPTH_TEST);
	glClear (GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glBlendFunc (GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	glUseProgram (shaderProg.getHandle ());

	glBindBuffer (GL_ELEMENT_ARRAY_BUFFER, iboCubeElements);
	int size;
	glGetBufferParameteriv (GL_ELEMENT_ARRAY_BUFFER, GL_BUFFER_SIZE, &size);

	glEnableVertexAttribArray (attr_coord3d);
	glVertexAttribPointer (attr_coord3d, 3, GL_FLOAT, GL_FALSE, ColorVertex::coordStride, 0);
	glEnableVertexAttribArray (attr_vcolor);
	glVertexAttribPointer (attr_vcolor, 3, GL_FLOAT, GL_FALSE, ColorVertex::colorStride, (GLvoid *)ColorVertex::colorOffset);

	glDrawElements (GL_TRIANGLES, (size / sizeof(GLushort)), GL_UNSIGNED_SHORT, 0);
	glDisableVertexAttribArray (attr_coord3d);
	glutSwapBuffers ();
}

void onThink () {
	auto elapsedTime = glutGet (GLUT_ELAPSED_TIME);
	auto angle = elapsedTime / 1000.0f * 45;
	glm::vec3 axis_y (0.0f, 1.0f, 0.0f);
	//glm::mat4
	auto rotAnim = glm::rotate (glm::mat4 (1.0f), angle, axis_y);
	auto model = glm::translate (glm::mat4 (1.0f), glm::vec3 (0.0f, 0.0f, -4.0f));
	auto view = glm::lookAt (glm::vec3 (0.0f, 2.0f, 0.0f), glm::vec3 (0.0f, 0.0f, -4.0f), glm::vec3 (0.0f, 1.0f, 0.0f));
	auto projection = glm::perspective (45.0f, 1.0f * screenWidth / screenHeight, 0.1f, 10.0f);
	auto mvp = projection * view * model * rotAnim;

	glUseProgram (shaderProg.getHandle ());
	glUniformMatrix4fv (uni_mvp, 1, GL_FALSE, glm::value_ptr (mvp));
	
	glutPostRedisplay ();
}

void onResize (int width, int height) {
	screenWidth = width;
	screenHeight = height;
	glViewport (0, 0, screenWidth, screenHeight);
}

void shutdown () {
	//glDeleteProgram (shaderProg.getHandle ());
	shaderProg.unload ();
	glDeleteBuffers (1, &vboCube);
	glDeleteBuffers (1, &iboCubeElements);
}

int main (int argc, char ** argv) {
	glutInit (&argc, argv);
	glutInitDisplayMode (GLUT_RGBA | GLUT_ALPHA | GLUT_DOUBLE | GLUT_DEPTH);
	glutInitWindowSize (screenWidth, screenHeight);
	glutCreateWindow ("OGL glut Demo");

	GLenum glew_status = glewInit ();
	if (glew_status != GLEW_OK) {
		gLog.log ("[r]!ERROR![/] %s", glewGetErrorString (glew_status));
		return 1;
	}

	if (!GLEW_VERSION_2_0) {
		gLog.log ("[r]!ERROR![/] Your GPU doesn't support OpenGL 2.0");
		return 1;
	}

	if (init () == 1) {
		glutDisplayFunc (onDisplay);
		glutIdleFunc (onThink);
		glutReshapeFunc (onResize);
		glutMainLoop ();
	}

	shutdown ();

	return 0;
}
