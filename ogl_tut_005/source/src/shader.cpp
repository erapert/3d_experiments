#include "shader.hpp"
#include <cstdio>

#include "Logger.h"
extern Logger gLog;

#define INVALIDHANDLE -1

Shader::Shader () : shadHandle(INVALIDHANDLE) {
}

Shader::Shader (const char * shaderSource, GLenum shaderType) {
	load (shaderSource, shaderType);
}

Shader::~Shader () {
	unload ();
}

GLuint Shader::load (const char * shaderSource, GLenum shaderType) {
	if ((shaderSource == NULL) || (strlen (shaderSource) == 0)) {
		return INVALIDHANDLE;
	}

	//gLog.log ("shader source:")->log (shaderSource);

	// WARNING: the program will break right here if you didn't init glew before doing this.
	shadHandle = glCreateShader (shaderType);
	glShaderSource (shadHandle, 1, (const GLchar **)&shaderSource, NULL);

	auto compiledOK = GL_FALSE;
	glCompileShader (shadHandle);
	glGetShaderiv (shadHandle, GL_COMPILE_STATUS, &compiledOK);
	
	if (compiledOK == GL_FALSE) {
		gLog.log ("[r]!ERROR![/]:")->indent(1);
		logErrs (shadHandle);
		unload ();
		return INVALIDHANDLE;
	}

	return shadHandle;
}

void Shader::unload () {
	if (shadHandle != INVALIDHANDLE) {
		glDeleteShader (shadHandle);
		shadHandle = INVALIDHANDLE;
	}
}

void Shader::logErrs (GLuint shaderObj) {
	#ifdef LOGGING_ENABLED
		GLint logLen = 0;
		if (glIsShader (shaderObj)) {
			glGetShaderiv (shaderObj, GL_INFO_LOG_LENGTH, &logLen);
			char * err = new char [logLen];
			glGetShaderInfoLog (shaderObj, logLen, NULL, err);
			gLog.log ("[b]Shader:[/] %s", err);
			delete [] err;
		} else {
			gLog.log ("[r]!ERROR![/] Shader::logErrs(%d) -- Not an OpenGL shader or shader program.", shaderObj);
			return;
		}
	#endif
}
