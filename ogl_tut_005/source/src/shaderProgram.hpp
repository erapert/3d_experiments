#ifndef SHADERPROGRAM_H
#define SHADERPROGRAM_H

#include <GL/glew.h>
#include <GL/glut.h>

#include "shader.hpp"

class ShaderProgram {
	public:
		enum ShaderVarTypes { ATTRIB, UNIFORM };

	public:
		ShaderProgram ();
		~ShaderProgram ();

		GLuint			load				(const char * vertShaderName = NULL, const char * fragShaderName = NULL);
		bool			unload				();
		GLuint			getHandle			();
		bool			connectVar			(ShaderVarTypes type, GLint & varHandle, const char * varName);
		static void		logErrs				(const GLuint & shadProgHandle);
		static void		logErrs				(const ShaderProgram & other);

	private:
		ShaderProgram (const ShaderProgram & other);
		ShaderProgram * copy (const ShaderProgram & other);
		ShaderProgram * operator = (const ShaderProgram & other);

	protected:
						GLuint				progHandle;
						Shader				vertShad;
						Shader				fragShad;
};

inline GLuint ShaderProgram::getHandle () {
	return progHandle;
}

inline void ShaderProgram::logErrs (const ShaderProgram & other) {
	logErrs (other.progHandle);
}

#endif
