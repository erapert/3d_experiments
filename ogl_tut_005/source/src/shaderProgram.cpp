#include "shaderProgram.hpp"
#include "fileContents.hpp"
#include "Logger.h"
extern Logger gLog;

#define INVALIDHANDLE -1

ShaderProgram::ShaderProgram () : progHandle(INVALIDHANDLE) {
}

ShaderProgram::~ShaderProgram () {
}

GLuint ShaderProgram::load (const char * vertShaderName, const char * fragShaderName) {
	if ((vertShaderName == NULL) || (fragShaderName == NULL)) {
		return INVALIDHANDLE;
	}

	FileContents fc;
	if (!fc.load (vertShaderName) || !vertShad.load (fc.getContents (), GL_VERTEX_SHADER)) {
		#ifdef LOGGING_ENABLED
			gLog.log ("[r]!ERROR![/] Failed to load shader file '%s'", vertShaderName);
		#endif
		return INVALIDHANDLE;
	}
	if (!fc.load (fragShaderName) || !fragShad.load (fc.getContents (), GL_FRAGMENT_SHADER)) {
		#ifdef LOGGING_ENABLED
			gLog.log ("[r]!ERROR![/] Failed to load shader file '%s'", fragShaderName);
		#endif
		return INVALIDHANDLE;
	}

	progHandle = glCreateProgram ();
	glAttachShader (progHandle, vertShad.getHandle ());
	glAttachShader (progHandle, fragShad.getHandle ());
	glLinkProgram (progHandle);
	auto linkedOK = GL_FALSE;
	glGetProgramiv (progHandle, GL_LINK_STATUS, &linkedOK);
	if (!linkedOK) {
		#ifdef LOGGING_ENABLED
			gLog.log ("[r]!ERROR![/] Failed to link shader program (%s, %s)", __FILE__, __LINE__);
		#endif
		return INVALIDHANDLE;
	}
	// after linking into a single shader prog it's just a waste of memory to keep these
	// sitting around on the GPU so let's free up that memory.
	glDetachShader (progHandle, vertShad.getHandle ());
	vertShad.unload ();
	glDetachShader (progHandle, fragShad.getHandle ());
	fragShad.unload ();
}

bool ShaderProgram::unload () {
	glDeleteProgram (progHandle);
	progHandle = INVALIDHANDLE;
	return true;
}

void ShaderProgram::logErrs (const GLuint & shadProgHandle) {
	#ifdef LOGGING_ENABLED
		if (shadProgHandle != INVALIDHANDLE) {
			GLint logLen;
			glGetProgramiv (shadProgHandle, GL_INFO_LOG_LENGTH, &logLen);
			char * errLog = new char [logLen];
			glGetProgramInfoLog (shadProgHandle, logLen, NULL, errLog);
			gLog.log ("[b]Shader Program:[/] %s", errLog);
			delete [] errLog;
		} else {
			gLog.log ("[r]!ERROR![/] Attempted to show errors for invalid shader program handle (%s, %s).", __FILE__, __LINE__);
		}
	#endif
}

bool ShaderProgram::connectVar (ShaderVarTypes type, GLint & varHandle, const char * varName) {
	switch (type) {
		case ATTRIB:
			varHandle = glGetAttribLocation (progHandle, varName);
			break;
		case UNIFORM:
			varHandle = glGetUniformLocation (progHandle, varName);
			break;
		default:
			#ifdef LOGGING_ENABLED
				gLog.log ("[r]!ERROR![/] Invalid attribute type passed to attachVarToShader()");
			#endif
			return false;
			break;
	}
	if (varHandle == INVALIDHANDLE) {
		#ifdef LOGGING_ENABLED
			gLog.log ("[y]!WARNING![/] Could not bind variable '%s'. Is it being used in your shader program?", varName);
		#endif
		return false;
	}
	return true;
}
