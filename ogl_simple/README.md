OGL Simple
==========

Simple/trivial OpenGL program which opens a window and draws a triangle.

Requirements
------------
The headers and libs for [FreeGlut](http://freeglut.sourceforge.net/index.php) and [Glew](http://glew.sourceforge.net/index.html) should already be in the proper places. If things fail to compile for you then double check VisualStudio's project settings for where to find the files:

* Project Properties
    - C/C++
        + General
            * Additional Include Directories
    - Linker
        + General
            * Additional Library Directories
        + Input
            * Additional Dependencies

Also note that the `.dll` files `freeglut.dll` and `glew32.dll` should both be inside the same dir as the compiled `.exe` when trying to run the program from VisualStudio (i.e. for debugging).

Compiling
---------
####Windows
#####Compile:
1. Open up the `.sln` file in [Visual Studio 2012](http://www.microsoft.com/visualstudio/eng/downloads#d-2012-express)
2. Hit `F7` to build.
3. Hit `F5` to run in debug mode.