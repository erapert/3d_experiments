#include <iostream>
#include <stdlib.h>
using namespace std;

#include <GL/glew.h>
#include <GL/glut.h>

GLuint shaderProgram;
GLint attributeCoord2d;

int init () {
	GLint shaderInittedOK = GL_FALSE;
	/* vertex shader */
	GLuint vs = glCreateShader (GL_VERTEX_SHADER);
	const char * vsSrc =
		"#version 120\n"
		"attribute vec2 coord2d;"
		"void main (void) {"
		"	gl_Position = vec4 (coord2d, 0.0, 1.0);"
		"}";
	glShaderSource (vs, 1, &vsSrc, NULL);
	glCompileShader (vs);
	glGetShaderiv (vs, GL_COMPILE_STATUS, &shaderInittedOK);
	if (!shaderInittedOK) {
		cout << "!ERROR! Failed to compile vertex shader." << endl;
		return 0;
	}

	/* fragment shader */
	GLuint fs = glCreateShader (GL_FRAGMENT_SHADER);
	const char * fsSrc =
		"#version 120\n"
		"void main (void) {"
		"	gl_FragColor[0] = 0.0;"
		"	gl_FragColor[1] = 0.0;"
		"	gl_FragColor[2] = 1.0;"
		"}";
	glShaderSource (fs, 1, &fsSrc, NULL);
	glCompileShader (fs);
	glGetShaderiv (fs, GL_COMPILE_STATUS, &shaderInittedOK);
	if (!shaderInittedOK) {
		cout << "!ERROR! Failed to compile fragment shader." << endl;
		return 0;
	}

	/* the shader program is a complete chain of vertex to fragment shaders */
	shaderProgram = glCreateProgram ();
	glAttachShader (shaderProgram, vs);
	glAttachShader (shaderProgram, fs);
	glLinkProgram (shaderProgram);
	glGetProgramiv (shaderProgram, GL_LINK_STATUS, &shaderInittedOK);
	if (!shaderInittedOK) {
		cout << "!ERROR! Failed to link GL shader program." << endl;
		return 0;
	}

	/* triangle verts */
	const char * attrName = "coord2d";
	attributeCoord2d = glGetAttribLocation (shaderProgram, attrName);
	if (attributeCoord2d == -1) {
		cout << "!ERROR! Failed to bind attribute '" << attrName << "'" << endl;
		return 0;
	}

	return 1;
}

void onDisplay () {
	glClearColor (1.0, 1.0, 1.0, 1.0);
	glClear (GL_COLOR_BUFFER_BIT);

	glUseProgram (shaderProgram);
	glEnableVertexAttribArray (attributeCoord2d);
	GLfloat triVerts [] = {
		0.0f, 0.8f,
		-0.8f, -0.8f,
		0.8f, -0.8f
	};

	glVertexAttribPointer (
		attributeCoord2d,		// the attr
		2,						// number of elements per vertex
		GL_FLOAT,				// type of each element
		GL_FALSE,				// take my values as they are
		0,						// no extra data between each vert
		triVerts				// the buffer of verts
	);

	glDrawArrays (GL_TRIANGLES, 0, 3);
	glDisableVertexAttribArray (attributeCoord2d);
	glutSwapBuffers ();
}

void shutdown () {
	glDeleteProgram (shaderProgram);
}

int main (int argc, char ** argv) {
	glutInit (&argc, argv);
	glutInitDisplayMode (GLUT_RGBA | GLUT_DOUBLE | GLUT_DEPTH);
	glutInitWindowSize (800, 1280);
	glutCreateWindow ("OGL glut Demo");

	GLenum glew_status = glewInit ();
	if (glew_status != GLEW_OK) {
		cout << "!ERROR! " << glewGetErrorString (glew_status) << endl;
		return 1;
	}

	if (init () == 1) {
		glutDisplayFunc (onDisplay);
		glutMainLoop ();
	}

	shutdown ();
	return 0;
}
