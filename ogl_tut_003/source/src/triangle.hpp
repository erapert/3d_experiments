#ifndef TRIANGLE_H
#define TRIANGLE_H 1

#include <GL/glew.h>
#include <GL/glut.h>

/*
	for now this is just a bunch of utility stuff
*/

// a real mesh wouldn't be storing color per-vertex
// but for now it's convenient
struct ColorVertex {
	GLfloat coord3d[3];
	GLfloat vertColors[3];
	static const unsigned int coordStride = sizeof(GLfloat) * 6;
	static const unsigned int coordOffset = 0;
	// note the stride size-- it's not every 3 floats that you find a new color field. It's every SIX floats
	// because it needs to start at the offset and then count 6 before assuming that it's found another color
	static const unsigned int colorStride = sizeof(GLfloat) * 6;
	static const unsigned int colorOffset = sizeof(GLfloat) * 3;
};

// used to describe var types for binding to shaders
enum shaderVarTypes { ATTRIB, UNIFORM };
// wrapper for binding C++ vars to GLSL shaders so we can muck with 'em
bool attachVarToShader (shaderVarTypes type, GLint & attrHandle, const GLuint & shadProgram, const char * attrName);

#endif
