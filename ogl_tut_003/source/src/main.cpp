#include <iostream>
#include <stdlib.h>
#define _USE_MATH_DEFINES
#include <math.h>
using namespace std;

#include <GL/glew.h>
#include <GL/glut.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

#include "Logger.h"
#include "shader.hpp"
#include "fileContents.hpp"
#include "triangle.hpp"

// globals because building a real architecture is beyond the scope of these projects
GLuint shaderProgram;
GLuint vboTri;
GLint attr_coord3d, attr_vcolor;
GLint uniform_fade, uniform_transform;

// global var, because a singleton is a global in disguise anyway!
Logger gLog = Logger (2, true);


int init () {
	GLint shaderInittedOK = GL_FALSE;

	// load up the vert and frag shaders
	string vertShadFileName ("shaders/foo.vert");
	string fragShadFileName ("shaders/foo.frag");
	FileContents fc;
	Shader foovert;
	Shader foofrag;
	if (!fc.load (vertShadFileName) || !foovert.load (fc.getContents(), GL_VERTEX_SHADER)) {
		gLog.log ("[r]!ERROR![/] Failed to load shader file '%s'", vertShadFileName.c_str ());
	}
	if (!fc.load (fragShadFileName) || !foofrag.load (fc.getContents(), GL_FRAGMENT_SHADER)) {
		gLog.log ("[r]!ERROR![/] Failed to load shader file '%s'", fragShadFileName.c_str ());
	}
	// the shader program is a complete chain of vertex to fragment shaders
	shaderProgram = glCreateProgram ();
	glAttachShader (shaderProgram, foovert.getHandle());
	glAttachShader (shaderProgram, foofrag.getHandle());
	glLinkProgram (shaderProgram);
	glGetProgramiv (shaderProgram, GL_LINK_STATUS, &shaderInittedOK);
	if (!shaderInittedOK) {
		cout << "!ERROR! Failed to link GL shader program." << endl;
		return 0;
	}

	// triangle
	struct ColorVertex tri [] = {
		{{0.0f, 0.8f, 0.0f}, {1.0f, 0.0f, 0.0f}},
		{{-0.8f, -0.8f, 0.0f}, {0.0f, 1.0f, 0.0f}},
		{{0.8f, -0.8f, 0.0f}, {0.0f, 0.0f, 1.0f}}
	};
	glGenBuffers (1, &vboTri);
	glBindBuffer (GL_ARRAY_BUFFER, vboTri);
	glBufferData (GL_ARRAY_BUFFER, sizeof(tri), tri, GL_STATIC_DRAW);

	attachVarToShader (ATTRIB, attr_coord3d, shaderProgram, "coord3d");
	attachVarToShader (ATTRIB, attr_vcolor, shaderProgram, "vcolor");
	attachVarToShader (UNIFORM, uniform_fade, shaderProgram, "uniFade");
	attachVarToShader (UNIFORM, uniform_transform, shaderProgram, "uniTransform");

	return 1;
}

void onDisplay () {
	glClearColor (1.0, 1.0, 1.0, 1.0);
	glClear (GL_COLOR_BUFFER_BIT);
	glEnable (GL_BLEND);
	glBlendFunc (GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	glUseProgram (shaderProgram);

	glBindBuffer (GL_ARRAY_BUFFER, vboTri);
	glEnableVertexAttribArray (attr_coord3d);
	glVertexAttribPointer (attr_coord3d, 3, GL_FLOAT, GL_FALSE, ColorVertex::coordStride, 0);
	glEnableVertexAttribArray (attr_vcolor);
	glVertexAttribPointer (attr_vcolor, 3, GL_FLOAT, GL_FALSE, ColorVertex::colorStride, (GLvoid *)ColorVertex::colorOffset);

	glDrawArrays (GL_TRIANGLES, 0, 3);
	glDisableVertexAttribArray (attr_coord3d);
	glutSwapBuffers ();
}

void onThink () {
	auto elapsedTime = glutGet (GLUT_ELAPSED_TIME);

	// color alpha from 0.0 <-> 1.0 every 5 seconds
	float currFade = (float)sinf (elapsedTime / 1000.0f * (2 * (float)M_PI) / 5) / 2 + 0.5f;
	// move -1 <-> 1 every 5 seconds
	float move = (float)sinf (elapsedTime / 1000.0f * (2 * (float)M_PI) / 5);
	// rotation 45 degrees per second
	float angle = elapsedTime / 1000.0f * 45;
	
	glm::vec3 axis_z (0, 0, 1);
	// remember, with matrixes the order of multiplication is important
	// so we go from right to left (here it's bottom to top because of the way it's formatted/written)
	glm::mat4 transform = (
		glm::translate (glm::mat4 (1.0f), glm::vec3(move, 0.0f, 0.0f))
		*
		glm::rotate (glm::mat4(1.0f), angle, axis_z)
	);

	glUseProgram (shaderProgram);
	glUniform1f (uniform_fade, currFade);
	glUniformMatrix4fv (uniform_transform, 1, GL_FALSE, glm::value_ptr (transform));
	
	glutPostRedisplay ();
}

void shutdown () {
	glDeleteProgram (shaderProgram);
	glDeleteBuffers (1, &vboTri);
}

int main (int argc, char ** argv) {
	glutInit (&argc, argv);
	glutInitDisplayMode (GLUT_RGBA | GLUT_ALPHA | GLUT_DOUBLE | GLUT_DEPTH);
	glutInitWindowSize (640, 480);
	glutCreateWindow ("OGL glut Demo");

	GLenum glew_status = glewInit ();
	if (glew_status != GLEW_OK) {
		gLog.log ("[r]!ERROR![/] %s", glewGetErrorString (glew_status));
		return 1;
	}

	if (!GLEW_VERSION_2_0) {
		gLog.log ("[r]!ERROR![/] Your GPU doesn't support OpenGL 2.0");
		return 1;
	}

	if (init () == 1) {
		glutDisplayFunc (onDisplay);
		glutIdleFunc (onThink);
		glutMainLoop ();
	}

	shutdown ();

	return 0;
}
