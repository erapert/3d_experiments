#version 120

// comes in from the vert shader
varying vec3 fcolor;

uniform float uniFade;

void main () {
	gl_FragColor = vec4 (fcolor.x, fcolor.y, fcolor.z, uniFade);
}
