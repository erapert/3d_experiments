#version 120

uniform mat4 uniTransform;

attribute vec3 coord3d;
attribute vec3 vcolor;

// used to pass color to the fragment shader
// it has to be spelled the same in both places
varying vec3 fcolor;

void main () {
	fcolor = vcolor;
	gl_Position = uniTransform * vec4 (coord3d, 1.0);
}
