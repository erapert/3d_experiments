ogl_tut_003
============
A very simple implementation following the tutorials [here](http://en.wikibooks.org/wiki/OpenGL_Programming/Modern_OpenGL_Tutorial_03) and [here](http://en.wikibooks.org/wiki/OpenGL_Programming/Modern_OpenGL_Tutorial_04)

So this is a combination of both tutorial_03 and tutorial_04.

###Features
* shader class for handling shaders (not entire shader programs though!)
* file loader for loading shaders written in separate files
* trivial abstraction of a colored vertex (used for building a triangle)
* trivial function for binding C++ variables to handles of shader variables (uniforms and attributes)
* animation: the triangle moves left and right and fades in and out

Yippee!