#ifndef WIN_COMPAT_H
#define WIN_COMPAT_H 1

/*
	Compatibility/replacements for common windows stuff
*/

#include <stdio.h>
#include <sys/select.h>
#include <termios.h>
#include <stropts.h>
#include <sys/ioctl.h>

// A simple drop-in replacement for _kbhit() from windows.
int kbhit ();

#endif

