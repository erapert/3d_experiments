#include <iostream>
#include <unistd.h>
using namespace std;

#include "OVR.h"
using namespace OVR;

#include "win_compat.h" // windows compatibility stuff (i.e. kbhit())

/* quick and dirty architecture */
Ptr <DeviceManager>		manager;
Ptr <HMDDevice>			hmd;
Ptr <SensorDevice>		sensor;
SensorFusion			fusion;
HMDInfo					hmdInfo;
bool					infoLoaded;

void displayInfo () {
	cout << "\t----- HMD info ----- " << endl;
	cout << "\tDisplayDeviceName: " << hmdInfo.DisplayDeviceName << endl;
	cout << "\tProductName: " << hmdInfo.ProductName << endl;
	cout << "\tManufacturer: " << hmdInfo.Manufacturer << endl;
	cout << "\tVersion: " << hmdInfo.Version << endl;
	cout << "\tHResolution: " << hmdInfo.HResolution<< endl;
	cout << "\tVResolution: " << hmdInfo.VResolution<< endl;
	cout << "\tHScreenSize: " << hmdInfo.HScreenSize<< endl;
	cout << "\tVScreenSize: " << hmdInfo.VScreenSize<< endl;
	cout << "\tVScreenCenter: " << hmdInfo.VScreenCenter<< endl;
	cout << "\tEyeToScreenDistance: " << hmdInfo.EyeToScreenDistance << endl;
	cout << "\tLensSeparationDistance: " << hmdInfo.LensSeparationDistance << endl;
	cout << "\tInterpupillaryDistance: " << hmdInfo.InterpupillaryDistance << endl;
	cout << "\tDistortionK[0]: " << hmdInfo.DistortionK[0] << endl;
	cout << "\tDistortionK[1]: " << hmdInfo.DistortionK[1] << endl;
	cout << "\tDistortionK[2]: " << hmdInfo.DistortionK[2] << endl;
	cout << "\t--------------------------" << endl;
}

bool init () {
	cout << "Init..." << endl;
	
	System::Init ();
	manager = *DeviceManager::Create ();
	hmd = *manager->EnumerateDevices<HMDDevice>().CreateDevice();
	
	// if the display is present then get its info and get the sensor from the display
	if (hmd) {
		cout << "\tFound HMDDevice (the screen). Getting info..." << endl;
		infoLoaded = hmd->GetDeviceInfo (&hmdInfo);
		if (infoLoaded) {
			displayInfo ();
		} else {
			cout << "\t!WARNING! Failed to find HMDDevice screen info." << endl;
		}
		cout << "\tGetting sensor..." << endl;
		sensor = *hmd->GetSensor ();
	
	// otherwise just get the sensor by itself
	} else {
		cout << "\t!WARNING! Failed to get HMDDevice (the screen)... is it plugged in and turned on?" << endl;
		cout << "\tSearching for sensor..." << endl;
		sensor = *manager->EnumerateDevices<SensorDevice>().CreateDevice ();
	}
	
	if (sensor) {
		cout << "\tAttaching to sensor..." << endl;
		fusion.AttachToSensor (sensor);
	} else {
		cout << "\t!ERROR! Failed to init sensor." << endl;
		return false;
	}
	return true;
}

void output () {
	cout << "output..." << endl;
	cout << endl << "\tPress ENTER to continue" << endl;
	cin.get ();
	
	while ((sensor) && (!kbhit ())) {
		Quatf quat = fusion.GetOrientation ();
		float yaw = 0.0f;
		float pitch = 0.0f;
		float roll = 0.0f;
		
		quat.GetEulerAngles <Axis_Y, Axis_X, Axis_Z> (&yaw, &pitch, &roll);
		cout << " Yaw: " << RadToDegree (yaw)
			<< ", Pitch: " << RadToDegree (pitch)
			<< ", Roll: " << RadToDegree (roll)
			<< endl;
		
		usleep (50000);
	}
}

void shutdown () {
	cout << "shutdown..." << endl;
	
	cout << "\tclear sensor: ";
	sensor.Clear ();
	cout << " OK" << endl << "\tclear hmd: ";
	hmd.Clear ();
	cout << " OK" << endl << "\tclear manager: ";
	manager.Clear ();
	cout << "OK" << endl << "\tsystem destroy: ";
	System::Destroy ();
	cout << "OK" << endl;
}

int main () {
	if (init ()) {
		output ();
	}
	shutdown ();
	cout << "done." << endl;
	return 0;
}

