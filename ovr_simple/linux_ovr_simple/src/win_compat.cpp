#include "win_compat.h"

int kbhit () {
	static const int STDIN = 0;
	static bool initialized = false;
	
	if (!initialized) {
		// use termios to turn off line buffering
		termios term;
		tcgetattr (STDIN, &term);
		term.c_lflag &= ~ICANON;
		tcsetattr (STDIN, TCSANOW, &term);
		setbuf (stdin, NULL);
		initialized = true;
	}
	
	int bytesWaiting;
	ioctl (STDIN, FIONREAD, &bytesWaiting);
	return bytesWaiting;
}

