#include "SimpleOVROutput.h"

int main () {
	SimpleOVROutput soo;

	if (soo.init ()) {
		soo.run ();
	}
	soo.shutdown ();

	return 0;
}