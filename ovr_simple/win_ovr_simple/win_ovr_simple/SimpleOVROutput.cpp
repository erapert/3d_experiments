#include <iostream>
#include <conio.h>		// _kbhit()
using namespace std;

#include "SimpleOVROutput.h"
using namespace OVR;

SimpleOVROutput::SimpleOVROutput (void) {
}

SimpleOVROutput::~SimpleOVROutput (void) {
}

bool SimpleOVROutput::init () {
	cout << "Init:" << endl;
	System::Init ();
	manager = *DeviceManager::Create ();
	hmd = *manager->EnumerateDevices<HMDDevice>().CreateDevice();
	if (hmd) {
		cout << "\tFound HMD. Getting info..." << endl;
		infoLoaded = hmd->GetDeviceInfo (&hmdInfo);
		if (infoLoaded) {
			printHMDInfo ();
		} else {
			cout << "\t!WARNING! Failed to find HMD info." << endl;
		}
		cout << "\tGetting sensor..." << endl;
		/*
			warning:
				hmd->GetSensor() will work with no errors but everything will hang on shutdown.
				So you MUST assign sensor to *hmd->GetSensor().
		*/
		sensor = *hmd->GetSensor ();
	} else {
		cout << "\t!WARNING! Failed to get HMD... is it plugged in and turned on?" << endl;
		cout << "\tSearching for sensor..." << endl;
		sensor = *manager->EnumerateDevices<SensorDevice>().CreateDevice();
	}

	if (sensor) {
		cout << "\tAttaching to sensor..." << endl;
		sensorFusion.AttachToSensor (sensor);
	} else {
		cout << "\t!ERROR! Failed to init sensor." << endl;
		return false;
	}
	return true;
}

void SimpleOVROutput::run () {
	cout << "Running... Press 'n' to cancel, ENTER to continue" << endl;
	char run = cin.get ();
	if (run != 'n') {
		cout << "Press any key to exit." << endl;
		// windows specific stuff for preparing to print over the same line
		HANDLE stdoutHandle = GetStdHandle (STD_OUTPUT_HANDLE);
		CONSOLE_SCREEN_BUFFER_INFO buffInfo;
		GetConsoleScreenBufferInfo (stdoutHandle, &buffInfo);

		while ((sensor) && (!_kbhit ())) {
			Quatf quat = sensorFusion.GetOrientation ();
			float yaw, pitch, roll;
			quat.GetEulerAngles <Axis_Y, Axis_X, Axis_Z> (&yaw, &pitch, &roll);
			SetConsoleCursorPosition (stdoutHandle, buffInfo.dwCursorPosition);	// windows specific reset the cursor
			cout << "                                " << endl; // blank the line
			SetConsoleCursorPosition (stdoutHandle, buffInfo.dwCursorPosition);	// windows specific reset the cursor
			cout << "(" << RadToDegree (pitch) << ", " << RadToDegree (yaw) << ", " << RadToDegree (roll) << ")" << endl;
			Sleep (75);
		}
	}
}

void SimpleOVROutput::shutdown () {
	cout << "Shutdown..." << endl;
	cout << "clear sensor: ";
	sensor.Clear ();
	cout << " OK" << endl << "\tclear hmd: ";
	hmd.Clear ();
	cout << " OK" << endl << "\tclear manager: ";
	manager.Clear ();
	cout << "OK" << endl << "\tsystem destroy: ";
	System::Destroy ();
	cout << "OK" << endl;
}

void SimpleOVROutput::printHMDInfo () {
	cout << "\t----- HMD info ----- " << endl;
	cout << "\tDisplayDeviceName: " << hmdInfo.DisplayDeviceName << endl;
	cout << "\tProductName: " << hmdInfo.ProductName << endl;
	cout << "\tManufacturer: " << hmdInfo.Manufacturer << endl;
	cout << "\tVersion: " << hmdInfo.Version << endl;
	cout << "\tHResolution: " << hmdInfo.HResolution<< endl;
	cout << "\tVResolution: " << hmdInfo.VResolution<< endl;
	cout << "\tHScreenSize: " << hmdInfo.HScreenSize<< endl;
	cout << "\tVScreenSize: " << hmdInfo.VScreenSize<< endl;
	cout << "\tVScreenCenter: " << hmdInfo.VScreenCenter<< endl;
	cout << "\tEyeToScreenDistance: " << hmdInfo.EyeToScreenDistance << endl;
	cout << "\tLensSeparationDistance: " << hmdInfo.LensSeparationDistance << endl;
	cout << "\tInterpupillaryDistance: " << hmdInfo.InterpupillaryDistance << endl;
	cout << "\tDistortionK[0]: " << hmdInfo.DistortionK[0] << endl;
	cout << "\tDistortionK[1]: " << hmdInfo.DistortionK[1] << endl;
	cout << "\tDistortionK[2]: " << hmdInfo.DistortionK[2] << endl;
	cout << "\t--------------------------" << endl;
}
