#ifndef SIMPLEOVROUTPUT_H
#define SIMPLEOVROUTPUT_H 1

#include "OVR.h"

class SimpleOVROutput {
	public:
					SimpleOVROutput			(void);
					~SimpleOVROutput		(void);

		bool		init					();
		void		run						();
		void		shutdown				();
		void		printHMDInfo			();

	protected:
		void		printRots				(const float, const float, const float);

	private:
		OVR::Ptr <OVR::DeviceManager>	manager;
		OVR::Ptr <OVR::HMDDevice>		hmd;
		OVR::Ptr <OVR::SensorDevice>	sensor;
		OVR::SensorFusion				sensorFusion;
		OVR::HMDInfo					hmdInfo;
		bool							infoLoaded;
};

#endif
