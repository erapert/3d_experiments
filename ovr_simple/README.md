OVR Simple
==========

Simple/trivial implementation of the [minimal Oculus Rift program](https://developer.oculusvr.com/wiki/Minimal_Oculus_Application).

This is just a console program that'll print out the orientation of the headset. But you can use this to see how the head tracking works. Graphics are *not* covered here.

Compiling
---------

####Linux
#####Requirements:
* scons: `sudo apt-get install scons`
* Run the 'Rift install script to get all the other packages that `libovr` needs.

#####Compile:
1. *cd* into the same directory as `SConstruct`
2. run `scons`

####Windows
#####Compile:
1. Open up the `.sln` file in [Visual Studio 2012](http://www.microsoft.com/visualstudio/eng/downloads#d-2012-express)
2. Hit `F7` to build.
3. Hit `F5` to run in debug mode.